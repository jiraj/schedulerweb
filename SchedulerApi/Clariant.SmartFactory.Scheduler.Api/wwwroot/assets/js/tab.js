var tabs = (function () {
    return {
        close: () => $("#item-modal").hide("slow"),
        open: () => $("#item-modal").show(3000),
        bind: (itm) => {
            try {
                $("#mod-fg-desc").text(itm.finishedgooddescription);
                $("#modal-fg-number").text(itm.finishedgoodnumber);

                $("#mod-customer-name").text(itm.customername);
                $("#modal-ponum").text(itm.ponumber);
                $("#modal-ord-qty").text(itm.orderquantity);
                $("#modal-rm-date").text(formatDDMMM(itm.rawmaterialavailabledate));
                $("#modal-fg-date").text(formatDDMMM(itm.finishedgoodproductionenddate));
                $("#modal-start-date").text(formatDDMONYY(itm.start));
                $("#modal-start-time").text(formatAMPM(itm.start));
                $("#modal-end-time").text(formatAMPM(itm.end));
                tabs.open();
            } catch (ex) { console.log(ex) }
        }
    }
})();

$(document).ready(function () {
    $("#pills-tab a").on("click", function (e) {
        e.preventDefault();
        console.log("called");
        $(this).tab("show");
    });
    $(document).on("click", "#btnTabClose", function () {
        //$("#item-modal").hide("slow");
        tabs.close();
    });
});

function formatDDMMM(date) {
    const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    if (date) {
        date = new Date(date);
        return date.getDate() + " " + months[date.getMonth()];

    }
    else {
        return '';
    }
}

function randomNumber(min, max) {
    return Math.random() * (max - min) + min;
}

function timeDiffCalc(dateFuture, dateNow) {
    let difference = '';
    if (dateNow && dateFuture) {
        let diffInMilliSeconds = Math.abs(dateFuture - dateNow) / 1000;
        const days = Math.floor(diffInMilliSeconds / 86400);
        diffInMilliSeconds -= days * 86400;
        const hours = Math.floor(diffInMilliSeconds / 3600) % 24;
        diffInMilliSeconds -= hours * 3600;
        const minutes = Math.floor(diffInMilliSeconds / 60) % 60;
        diffInMilliSeconds -= minutes * 60;
        if (days > 0) {
            difference += (days === 1) ? `${days} day, ` : `${days} days, `;
        }
        if (days > 0 || hours > 0)
            difference += (hours === 0 || hours === 1) ? `${hours} hour, ` : `${hours} hours, `;
        difference += (minutes === 0 || hours === 1) ? `${minutes} minutes` : `${minutes} minutes`;
        difference = '(Duration : ' + difference + ')';
    }
    return difference;
}

function formatAMPM(date) {
    if (date) {
        date = new Date(date);
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0' + minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        return strTime;
    } else {
        return '';
    }
}

function formatDDMONYY(date) {
    const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    if (date) {
        date = new Date(date);
        return date.getDate() + "/" + months[date.getMonth()] + "/" + date.getFullYear().toString().substr(-2);

    }
    else {
        return '';
    }
}