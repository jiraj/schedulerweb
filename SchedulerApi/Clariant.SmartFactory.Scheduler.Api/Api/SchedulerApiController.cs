﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Clariant.SmartFactory.Scheduler.Api.Api
{
    [Route("api/schedule")]
    public class SchedulerApiController : Controller
    {
        // GET: api/<controller>
        [HttpGet]
        [Route("extruder")]
        public List<dynamic> Get(string start)
        {
            if (!DateTime.TryParse(start, out DateTime dt))
                dt = DateTime.Now;

            string sql = $@"select br.id as batchid
	,br.name as PONumber
	,fa.name as extrudername
	,br.scheduled_start_date_time
	,br.status
	,bts.name
	,brs.id as batchstepid
	,brs.expected_start_date_time as extrusion_start_date
	,brs.split_step_expected_end_date_time as extrusion_end_date
	,brse.facility_asset_id
from batch_run br
	left join batch_run_step brs on brs.batch_run_id = br.id and brs.is_deleted is not true
	left join batch_template_step bts on bts.id = brs.batch_template_step_id and bts.is_deleted is not true
	left join batch_run_step_equipment brse on brse.batch_run_step_id = brs.id and brse.is_deleted is not true
	left join facility_asset fa on fa.id = brse.facility_asset_id and fa.is_deleted is not true
where br.batch_run_type = 1 and br.is_deleted is not true
	and bts.name ilike '%extru%'
	order by br.id desc
limit 100";
            using (Npgsql.NpgsqlConnection con = new Npgsql.NpgsqlConnection("User ID=admin;Password=DRBLFXCSTCCLBIHU;Host=sl-eu-de-1-portal.8.dblayer.com;Port=23019;Database=CLARIANT_UEIOS_V3;"))
            {
                con.Open();
                using (Npgsql.NpgsqlCommand cmd = new Npgsql.NpgsqlCommand())
                {
                    cmd.Connection = con;
                    cmd.CommandText = sql;
                    using (var rdr = cmd.ExecuteReader())
                    {
                        System.Data.DataTable dtt = new System.Data.DataTable();
                        dtt.Load(rdr);
                        return Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(Newtonsoft.Json.JsonConvert.SerializeObject(dtt));
                    }
                }
            }
        }

        [HttpGet]
        [Route("extruder/sap")]
        public dynamic GetSap(string start)
        {
            if (!DateTime.TryParse(start, out DateTime dt))
                dt = DateTime.Now;

            string sql = $@"select br.id as batchid
	,br.name as PONumber
	,fa.name as extrudername
	,br.scheduled_start_date_time
	,br.status
	,bts.name
	,brs.id as batchstepid
	,brs.expected_start_date_time as extrusion_start_date
	,brs.split_step_expected_end_date_time as extrusion_end_date
	,brse.facility_asset_id
from batch_run br
	left join batch_run_step brs on brs.batch_run_id = br.id and brs.is_deleted is not true
	left join batch_template_step bts on bts.id = brs.batch_template_step_id and bts.is_deleted is not true
	left join batch_run_step_equipment brse on brse.batch_run_step_id = brs.id and brse.is_deleted is not true
	left join facility_asset fa on fa.id = brse.facility_asset_id and fa.is_deleted is not true
where br.batch_run_type = 1 and br.is_deleted is not true
	and bts.name ilike '%extru%'
	order by br.id desc
limit 100";
            using (Npgsql.NpgsqlConnection con = new Npgsql.NpgsqlConnection("User ID=admin;Password=DRBLFXCSTCCLBIHU;Host=sl-eu-de-1-portal.8.dblayer.com;Port=23019;Database=CLARIANT_UEIOS_V3;"))
            {
                con.Open();
                using (Npgsql.NpgsqlCommand cmd = new Npgsql.NpgsqlCommand())
                {
                    cmd.Connection = con;
                    cmd.CommandText = sql;
                    using (var rdr = cmd.ExecuteReader())
                    {
                        System.Data.DataTable dtt = new System.Data.DataTable();
                        dtt.Load(rdr);
                        var dff = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Extdata>>(Newtonsoft.Json.JsonConvert.SerializeObject(dtt));
                        return dff.Select(t => t.extrudername).Distinct().ToList().Select(t =>
                        new
                        {
                            machinename = t,
                            pic = "img/MachineExt_2x.png",
                            batches = dff.FindAll(t1 => t1.extrudername == t).ToList()
                        }).ToList();
                    }
                }
            }
        }

        [HttpGet]
        [Route("v1")]
        public dynamic GetV1(string start)
        {
            if (!DateTime.TryParse(start, out DateTime dt))
                dt = DateTime.Now;

            string sql = $@"select br.id as batchid
	,ibr.scheduled_delivery_date as SOScheduledDeliveryDate
	,ibr.scheduled_delivery_date as FinishedGoodProductionEndDate
    ,ibr.basic_finished_date as BasicFinishedDate
    ,ibr.eta as BasicStartDate
    ,ibr.description as SalesOrderItem
    ,ibr.document_number as SalesOrderNumber
    ,customer.id as customerID
    ,customer.name as CustomerName
    ,customer.customer_code as CustomerCode
    ,customer.delivery_address as CustomerCountry
    ,customer.notes as CustomerClassification
	,bom.ID as BOMID
    ,bom.bom_name as BOMName
    ,bom.bom_description as FinishedGoodBatchNumber
	,bom.quantity_to_make as OrderQuantity
    ,iit.ID as FinishedGoodId
    ,iit.name as FinishedGoodNumber
    ,iit.description as FinishedGoodDescription
    ,iit.grouping as RecipeGroup
	,br.name as PONumber
	,fa.name as extrudername
	,br.scheduled_start_date_time
	,br.status
    ,br.scheduled_date_time as RawMaterialAvailableDate
	,bts.name
	,brs.id as batchstepid
	,brs.expected_start_date_time as extrusion_start_date
	,brs.split_step_expected_end_date_time as extrusion_end_date
	,brse.facility_asset_id
from batch_run br
	LEFT JOIN internal_batch_run as ibr ON ibr.batch_run_link_id = br.ID
    LEFT JOIN customer as customer ON customer.ID = ibr.customer_link_id
	left join batch_run_step brs on brs.batch_run_id = br.id and brs.is_deleted is not true and brs.expected_start_date_time > now() - INTERVAL '10 DAYS'
	left join batch_template_step bts on bts.id = brs.batch_template_step_id and bts.is_deleted is not true
	left join batch_run_step_equipment brse on brse.batch_run_step_id = brs.id and brse.is_deleted is not true
	left join facility_asset fa on fa.id = brse.facility_asset_id and fa.is_deleted is not true
	LEFT JOIN bill_of_materials as bom ON bom.ID = br.custom_bill_of_materials_link_id
    LEFT JOIN inventory_item_type as iit ON iit.ID = bom.inventory_item_type_link_id
where br.batch_run_type = 1 and br.is_deleted is not true
	and bts.name ilike '%extru%'
	order by br.id desc
limit 1000";
            using (Npgsql.NpgsqlConnection con = new Npgsql.NpgsqlConnection("User ID=admin;Password=DRBLFXCSTCCLBIHU;Host=sl-eu-de-1-portal.8.dblayer.com;Port=23019;Database=CLARIANT_UEIOS_V3;"))
            {
                con.Open();
                using (Npgsql.NpgsqlCommand cmd = new Npgsql.NpgsqlCommand())
                {
                    cmd.Connection = con;
                    cmd.CommandText = sql;
                    using (var rdr = cmd.ExecuteReader())
                    {
                        System.Data.DataTable dtt = new System.Data.DataTable();
                        dtt.Load(rdr);
                        //return Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(Newtonsoft.Json.JsonConvert.SerializeObject(dtt));
                        var dff = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Uidatav1>>(Newtonsoft.Json.JsonConvert.SerializeObject(dtt));
                        return dff.Select(t => t.extrudername).Distinct().ToList().Select(t =>
                        new
                        {
                            machinename = t,
                            pic = "img/MachineExt_2x.png",
                            batches = dff.FindAll(t1 => t1.extrudername == t).ToList()
                        }).ToList();
                    }
                }
            }
        }
    }
    public class Uidatav1
    {
        public int batchid { get; set; }
        public DateTime soscheduleddeliverydate { get; set; }
        public DateTime finishedgoodproductionenddate { get; set; }
        public DateTime basicfinisheddate { get; set; }
        public DateTime basicstartdate { get; set; }
        public string salesorderitem { get; set; }
        public string salesordernumber { get; set; }
        public int customerid { get; set; }
        public string customername { get; set; }
        public string customercode { get; set; }
        public string customercountry { get; set; }
        public string customerclassification { get; set; }
        public int bomid { get; set; }
        public string bomname { get; set; }
        public string finishedgoodbatchnumber { get; set; }
        public double orderquantity { get; set; }
        public int finishedgoodid { get; set; }
        public string finishedgoodnumber { get; set; }
        public string finishedgooddescription { get; set; }
        public string recipegroup { get; set; }
        public string ponumber { get; set; }
        public string extrudername { get; set; }
        public DateTime? scheduled_start_date_time { get; set; }
        public int status { get; set; }
        public DateTime rawmaterialavailabledate { get; set; }
        public string name { get; set; }
        public int batchstepid { get; set; }
        public DateTime extrusion_start_date { get; set; }
        public DateTime extrusion_end_date { get; set; }
        public int? facility_asset_id { get; set; }
    }
    public class Extdata
    {
        public int batchid { get; set; }
        public int batchstepid { get; set; }
        public string extrudername { get; set; }
        public DateTime extrusion_end_date { get; set; }
        public DateTime extrusion_start_date { get; set; }
        public int facility_asset_id { get; set; }
        public string name { get; set; }
        public string ponumber { get; set; }
        public DateTime scheduled_start_date_time { get; set; }
        public int status { get; set; }
    }
}
