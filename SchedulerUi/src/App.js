import React, { Component } from "react";
import Header from "./Components/Header";
import Scheduer from "./Components/Schduler";
import NavBar from "./Components/NavBar";
import JobModal from "./Components/JobModal";

class App extends Component {
    render() {
        return (
            <div className="container-fluid">
                <Header />
                <NavBar />
                <Scheduer />
                <JobModal />
            </div>
        );
    }
}

export default App;
