﻿import React, { Component } from "react";

class JobModal extends Component {
    render() {
        return (
            <div id="item-modal" className="conatiner-fluid " style={{ position: "fixed", top: "160px", right: "10px", backgroundColor: "white", display: "none" }}>
                <div className="col-md-14">
                    <ul className="nav nav-pills mb-3" id="pills-tab" role="tablist">
                        <li className="nav-item">
                            <a className="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Job Detail</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Remark</a>
                        </li>
                    </ul>
                    <div className="tab-content" id="pills-tabContent">
                        <div className="tab-pane fade show active contentText" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                            <p>
                                <img src="/icons/icons-Product Code-XS.svg" alt="" /> <span id="mod-fg-desc"></span>
                            </p>
                            <p style={{ paddingLeft: "26px" }} id="modal-fg-number"></p>
                            <p>
                                <img src="/icons/icons-Customer-XS.svg" alt="" /> <span id="mod-customer-name"></span>
                            </p>
                            <p>
                                <img src="/icons/icons-Process Order-XS.svg" alt="" /> PO# <span id="modal-ponum"></span>
                            </p>
                            <p>
                                <img src="/icons/icons-Qty-XS.svg" alt="" /> <span id="modal-ord-qty"></span> kg Throughput 180 kg/h
                            </p>
                            <p className="batches">
                                <img src="/icons/icons-Batch-XS.svg" alt="" /> 5 Batches
                                <span className="status"></span>
                                <span className="color">Grey</span>
                            </p>
                            <p>
                                <img src="/icons/icons-calendar-XS.svg" alt="" /> RM: <span id="modal-rm-date"></span> - FG: <span id="modal-fg-date"></span>
                            </p>
                            <br />
                            <h4 className="headerText">Extrusion Scheduled</h4>
                            <p>
                                <img src="/icons/icons-calendar-XS.svg" alt="" /> <span id="modal-start-date"></span>
                                <img src="/icons/icons-Time-XS.svg" alt="" /> <span id="modal-start-time"></span> to <span id="modal-end-time"></span>
                            </p>
                            <br className="break" />
                            <br className="break" />
                            <p>
                                <button style={{ bottom: "5px" }} type="button" id="btnTabClose" className="btn btn-secondary">Close</button>
                            </p>
                        </div>
                        <div className="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                            <div>
                                First-time production, Food Safety & FDA
                    </div>
                            Copmliance Product
                    <p>Medical Safety</p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default JobModal;
