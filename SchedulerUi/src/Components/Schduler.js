import React, { Component } from "react";
import ReactDOM from 'react-dom'

class Scheduer extends React.Component {
    componentDidMount() {
        var groups = new window.vis.DataSet()
        var items = new window.vis.DataSet();
        //fetch('http://localhost:60696/api/schedule/v1')
        fetch('https://uical-test-v1.eu-de.mybluemix.net/api/schedule/v1')
            .then(response => response.json())
            .then(data => {
                //console.log(data);
                (data || []).forEach((itm, idx) => {
                    groups.add({
                        id: itm.machinename,
                        content: itm.machinename
                    });
                    (itm.batches || []).forEach((itm1, idx1) => {
                        //console.log(`${idx}-${idx1}`);
                        //console.log(parseInt(randomNumber(0, 4)));
                        items.add({
                            id: `${idx}-${idx1}`,
                            group: itm.machinename,
                            start: itm1.extrusion_start_date,
                            end: itm1.extrusion_end_date,
                            content: ``,
                            statcolr: ['#f0ad4e', '#5cb85c', '#d9534f'][parseInt(randomNumber(0, 3))],
                            ...itm1
                        });
                    });
                });
            });

        var options = {
            orientation: 'top',
            maxHeight: 630,
            start: new Date(),
            end: new Date(1000 * 60 * 60 * 24 + (new Date()).valueOf()),
            editable: false,
            showTooltips: true,
            verticalScroll: true,
            tooltip: {
                template: function (item, element, ddd) {
                    //console.log(item);
                    return `
        <div class='card' style=''>
            <div class='card-body'>
                <div class='card-details'>
                    <div class='card-text-right'>
                        <p>${item.finishedgoodnumber}</p>
                        <p>${item.ponumber}</p>
                    </div>
                    <div>
                        <p class='fw-900'>${item.finishedgooddescription}</p>
                        <p>${item.customername}</p>
                    </div>
                </div>
                <div class='card-details card-text-even'>
                    <p>${item.orderquantity} kg</p>
                    <p>Throughput 180 kg/h</p>
                    <p>5 Batches</p>
                    <p class='status'>
                        <span></span>Grey
                    </p>
                </div>
                <div class='card-details'>
                    <p>RM: ${formatDDMMM(item.rawmaterialavailabledate)} - FG: ${formatDDMMM(item.finishedgoodproductionenddate)}</p>
                </div>
                <div>
                    <p><b>${formatAMPM(item.start)} - ${formatAMPM(item.end)} <br />${timeDiffCalc(item.end, item.start)}</b></p>
                </div>
            </div>
        </div>`
                }
            },
            template: function (item, element) {
                if (!item) { return }
                ReactDOM.unmountComponentAtNode(element);
                return ReactDOM.render(
                    <ItemTemplate item={item} />, element);
            },
            groupTemplate: function (group, element) {
                if (!group) { return }
                ReactDOM.unmountComponentAtNode(element);
                return ReactDOM.render(
                    <GroupTemplate group={group} />, element);
            }
        }

        var container = document.getElementById('visualization');
        var timeline = new window.vis.Timeline(container, items, groups, options);
        timeline.on('select', function (props) {
            //alert('selected items: ' + props.items);
            //console.log(props);
            if (props && props.items && props.items[0]) {
                var itm = items.get(props.items[0])
                tabs.bind(itm);
                //console.log(itm);
            }
        });
    }
    render() {
        return (
            <div className="scheduler">
                <div className="container-fluid">
                    <div>
                        <div id="visualization"></div>
                    </div>)
                </div>
            </div>
        );
    }
}

export default Scheduer;

class GroupTemplate extends Component {
    render() {
        var { group } = this.props;
        return (
            <div>
                <img src="/icons/icons-Extruder.svg" alt="" /> <br />
                <label>{group.content}</label>
            </div>)
    }
};

class ItemTemplate extends Component {
    render() {
        var { item } = this.props;
        //console.log(item.start.getHours().toString().padStart(2, '0') + ":" + item.start.getMinutes().toString().padStart(2, '0'));
        //console.log(item.statcolr);
        return (
            <div className={'card'} style={{ borderLeft: '5px solid ' + item.statcolr }}>
                <div className={'card-body'}>
                    <div className={'card-details'}>
                        <div className={'card-text-right'}>
                            <p>{item.finishedgoodnumber}</p>
                            <p>{item.ponumber}</p>
                        </div>
                        <div>
                            <p className={'fw-900'}>{item.finishedgooddescription}</p>
                            <p>{item.customername}</p>
                        </div>
                    </div>
                    <div className={'card-details card-text-even'}>
                        <p>{item.orderquantity} kg</p>
                        <p>Throughput 180 kg/h</p>
                        <p>5 Batches</p>
                        <p className={'status'}>
                            <span></span>Grey
                    </p>
                    </div>
                    <div className={'card-details'}>
                        <p>RM: {formatDDMMM(item.rawmaterialavailabledate)} - FG: {formatDDMMM(item.finishedgoodproductionenddate)}</p>
                    </div>
                    <div>
                        <p><b>{formatAMPM(item.start)} - {formatAMPM(item.end)} {timeDiffCalc(item.end, item.start)}</b></p>
                    </div>
                </div>
            </div>)
    }
}

function formatDDMMM(date) {
    const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    if (date) {
        date = new Date(date);
        return date.getDate() + " " + months[date.getMonth()];

    }
    else {
        return '';
    }
}

function randomNumber(min, max) {
    return Math.random() * (max - min) + min;
}

function timeDiffCalc(dateFuture, dateNow) {
    let difference = '';
    if (dateNow && dateFuture) {
        let diffInMilliSeconds = Math.abs(dateFuture - dateNow) / 1000;
        const days = Math.floor(diffInMilliSeconds / 86400);
        diffInMilliSeconds -= days * 86400;
        const hours = Math.floor(diffInMilliSeconds / 3600) % 24;
        diffInMilliSeconds -= hours * 3600;
        const minutes = Math.floor(diffInMilliSeconds / 60) % 60;
        diffInMilliSeconds -= minutes * 60;
        if (days > 0) {
            difference += (days === 1) ? `${days} day, ` : `${days} days, `;
        }
        if (days > 0 || hours > 0)
            difference += (hours === 0 || hours === 1) ? `${hours} hour, ` : `${hours} hours, `;
        difference += (minutes === 0 || hours === 1) ? `${minutes} minutes` : `${minutes} minutes`;
        difference = '(Duration : ' + difference + ')';
    }
    return difference;
}

function formatAMPM(date) {
    if (date) {
        date = new Date(date);
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0' + minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        return strTime;
    } else {
        return '';
    }
}

function formatDDMONYY(date) {
    const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    if (date) {
        date = new Date(date);
        return date.getDate() + "/" + months[date.getMonth()] + "/" + date.getFullYear().toString().substr(-2);

    }
    else {
        return '';
    }
}

var tabs = (function () {
    var $ = window.$;
    return {
        close: () => $("#item-modal").hide("slow"),
        open: () => $("#item-modal").show(3000),
        bind: (itm) => {
            try {
                $("#mod-fg-desc").text(itm.finishedgooddescription);
                $("#modal-fg-number").text(itm.finishedgoodnumber);

                $("#mod-customer-name").text(itm.customername);
                $("#modal-ponum").text(itm.ponumber);
                $("#modal-ord-qty").text(itm.orderquantity);
                $("#modal-rm-date").text(formatDDMMM(itm.rawmaterialavailabledate));
                $("#modal-fg-date").text(formatDDMMM(itm.finishedgoodproductionenddate));
                $("#modal-start-date").text(formatDDMONYY(itm.start));
                $("#modal-start-time").text(formatAMPM(itm.start));
                $("#modal-end-time").text(formatAMPM(itm.end));
                tabs.open();
            } catch (ex) { console.log(ex) }
        }
    }
})();
var $ = window.$;
$(document).ready(function () {

    $("#pills-tab a").on("click", function (e) {
        e.preventDefault();
        console.log("called");
        $(this).tab("show");
    });
    $(document).on("click", "#btnTabClose", function () {
        //$("#item-modal").hide("slow");
        tabs.close();
    });
});