import React, { Component } from "react";
import {
    Button,
    Form,
    FormControl,
    Dropdown,
    DropdownButton,
} from "react-bootstrap";
import moment from "moment";
import Calendar from "../assets/icons/icons-Calendar.svg";
class Navbar extends Component {
    render() {
        return (
            <div className="Navbar">
                <div className="container-fluid">
                    <div>
                        <div className="manualList">
                            <Button variant="primary">
                                <i className="fa fa-calendar" aria-hidden="true"></i>Scheduler
                            </Button>
                            <Button variant="link">Manual List</Button>
                        </div>
                        <div className="calendar">
                            <Button variant="link">Today</Button>
                            <img src={Calendar} width="25" alt="Calendar" />
                            <h3 style={{ fontSize: "1em" }}> {moment().format(" D MMM YYYY ")}</h3>
                            <Form inline>
                                <FormControl type="text" placeholder="Search" />
                            </Form>
                            <DropdownButton
                                id="dropdown-basic-button"
                                className="currentShift"
                                title="Current Shift"
                            >
                                <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                                <Dropdown.Item href="#/action-2">Another action</Dropdown.Item>
                                <Dropdown.Item href="#/action-3">Something else</Dropdown.Item>
                            </DropdownButton>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Navbar;
