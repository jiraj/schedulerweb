import React, { Component } from "react";
import { Navbar, Nav } from "react-bootstrap";
import moment from "moment";
import UserProfile from "./UserProfile";
import Logo from "../assets/icons/logo-Avient.svg";

class Header extends Component {
  render() {
    return (
      <div className="wrapper">
        <Navbar expand="lg">
          <Navbar.Brand href="#home">
            <h3>
              <span className="Logo ">SmartFactory</span>
            </h3>
          </Navbar.Brand>
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ml-auto ">
              <Nav.Link href="#home" className="Moment">
                {moment().format("ddd D MMM hh:mm A ")}
              </Nav.Link>

              <UserProfile />
            </Nav>
            <Navbar.Brand href="#home" className="pl-4">
              <img alt="" src={Logo} className="surface1 " />
            </Navbar.Brand>
          </Navbar.Collapse>
        </Navbar>
      </div>
    );
  }
}
export default Header;
